#!/usr/bin/env python
# -*- coding:utf-8 -*-# 

# 骚神DP教学
# 电脑内需要提取安装谷歌浏览器或者其他chromium内核的浏览器  比如 edge浏览器  qq浏览器  360浏览器
# Drissionpage官网  http://drissionpage.cn/
# Drissionpage 版本需要大于等于 4.1.0.0
# 骚神网 https://wxhzhwxhzh.github.io/saossion_code_helper_online/

from DrissionPage import Chromium

# 连接浏览器并获取浏览器对象
browser = Chromium()  

# 获取标签页对象并打开网址
tab = browser.new_tab('https://drissionpage.cn/demos/iframe_diff_domain.html')


iframe = tab.get_frame('t:iframe') #最规范
ele = iframe.ele('网易首页')
print(ele)



iframe = tab.ele('t:iframe') #最简洁

ele = iframe('网易首页')
print(ele)



iframe = tab.eles('t:iframe')[0]  # 成功率最高
ele = iframe('网易首页')
print(ele)