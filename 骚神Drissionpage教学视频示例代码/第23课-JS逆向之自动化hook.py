
from DrissionPage import Chromium
from pathlib import Path

browser = Chromium(8976)
#创建空白页
tab=browser.new_tab()

def get_js_code(name:str):
    # 获取当前脚本所在的目录路径
    script_dir = Path(__file__).resolve().parent
    # 构建js文件的完整路径
    js_file = str(script_dir / name)
    # 读取并执行js文件内容
    with open(js_file, 'r', encoding='utf-8') as f:
        js_content = f.read()
        # print(js_content)
        return js_content


#插入初始化脚本
js=get_js_code('hook_tool.js')
tab.add_init_js(js)

# tab.add_init_js('禁用alert()')
# tab.add_init_js('禁用debugger()')
# tab.add_init_js('重写fetch2()')
# tab.add_init_js('重写xhrSend(to_json=true)')
tab.add_init_js('重写WebSocket原型()')


# input('已经运行注入hook脚本,输入任意键继续..')



tab.wait(4)
url2='https://www.runoob.com/try/try.php?filename=tryjs_alert'

url7='https://gitcode.com/'

yy='https://www.yy.com/54880976/54880976?tempId=16777217'

# url21='http://tool.pfan.cn/websocket'
# ws://124.222.224.186:8800

tab.get(yy)
tab.wait(10).run_js('覆写WebSocketOnMessage();')

input('输入任意键退出..')
