#  pyinstaller --onefile --icon=your_icon.ico your_script.py

import tkinter as tk
from tkinter import ttk
import subprocess

def start_chrome():
    # 获取用户输入的 Chrome 路径和调试端口
    chrome_path = chrome_path_entry.get()
    debugging_port = debugging_port_entry.get()
    
    # 启动 Chrome
    try:
        subprocess.Popen([chrome_path, f"--remote-debugging-port={debugging_port}"])
        status_label.config(text="Chrome started successfully!", foreground="green")
    except Exception as e:
        status_label.config(text=f"Error: {e}", foreground="red")

# 创建主窗口
root = tk.Tk()
root.title("从 Debugging 端口 启动浏览器")
root.geometry("600x300")  # 设置窗口大小为 600x300

# 创建样式
style = ttk.Style()
style.configure("TButton", padding=10, font=('宋体', 12))
style.configure("TLabel", font=('宋体', 12))


# 输入框：Chrome 路径
chrome_path_label = ttk.Label(root, text="浏览器 路径:")
chrome_path_label.pack(pady=5)
chrome_path_entry = ttk.Entry(root, width=20)
chrome_path_entry.insert(0, r"C:\Program Files\Google\Chrome\Application\chrome.exe")  # 默认值
chrome_path_entry.pack(pady=5,fill="x",padx=10)  # 自适应宽度

# 输入框：调试端口
debugging_port_label = ttk.Label(root, text="调试端口:")
debugging_port_label.pack(pady=5)
debugging_port_entry = ttk.Entry(root, width=15)
debugging_port_entry.insert(0, "9333")  # 默认值
debugging_port_entry.pack(pady=5)

# 创建按钮
start_button = ttk.Button(root, text="启动 浏览器", command=start_chrome)
start_button.pack(pady=20)

# 创建状态标签
status_label = ttk.Label(root, text="")
status_label.pack(pady=10)

# 运行主循环
root.mainloop()
