# VSCode 快捷键
## 通用
- Ctrl+Shift+P, F1：显示命令面板
- Ctrl+P：快速打开，转到文件...
- Ctrl+Shift+N：新建窗口/实例
- Ctrl+Shift+W：关闭窗口/实例
- Ctrl+,：用户设置
- Ctrl+K Ctrl+S：键盘快捷键

## 基本编辑
- Ctrl+X：剪切行（空选择时）
- Ctrl+C：复制行（空选择时）
- Alt+↑/↓：向上/下移动行
- Shift+Alt+↓/↑：向上/下复制行
- Ctrl+Shift+K：删除行
- Ctrl+Enter：在下方插入行
- Ctrl+Shift+Enter：在上方插入行
- Ctrl+Shift+\：跳转到匹配的括号
- Ctrl+]/[：增加/减少缩进
- Home/End：跳转到行首/尾
- Ctrl+Home：跳转到文件开头
- Ctrl+End：跳转到文件末尾
- Ctrl+↑/↓：向上/下滚动行
- Alt+PgUp/PgDn：向上/下滚动页面
- Ctrl+Shift+[：折叠（收起）区域
- Ctrl+Shift+]：展开（展开）区域
- Ctrl+K Ctrl+[：折叠（收起）所有子区域
- Ctrl+K Ctrl+]：展开（展开）所有子区域
- Ctrl+K Ctrl+0：折叠（收起）所有区域
- Ctrl+K Ctrl+J：展开（展开）所有区域
- Ctrl+K Ctrl+C：添加行注释
- Ctrl+K Ctrl+U：移除行注释
- Ctrl+/：切换行注释
- Shift+Alt+A：切换块注释
- Alt+Z：切换单词换行

## 导航
- Ctrl+T：显示所有符号
- Ctrl+G：转到行...
- Ctrl+P：转到文件...
- Ctrl+Shift+O：转到符号...
- Ctrl+Shift+M：显示问题面板
- F8：转到下一个错误或警告
- Shift+F8：转到上一个错误或警告
- Ctrl+Shift+Tab：导航编辑器组历史
- Alt+←/→：后退/前进
- Ctrl+M：切换Tab移动焦点

## 搜索和替换
- Ctrl+F：查找
- Ctrl+H：替换
- F3/Shift+F3：查找下一个/上一个
- Alt+Enter：选择所有查找匹配项
- Ctrl+D：将下一个查找匹配项添加到选择
- Ctrl+K Ctrl+D：将最后一个选择移动到下一个查找匹配项
- Alt+C/R/W：切换大小写敏感/正则表达式/全词匹配

## 多光标和选择
- Alt+Click：插入光标
- Ctrl+Alt+↑/↓：在上方/下方插入光标
- Ctrl+U：撤销最后一个光标操作
- Shift+Alt+I：在每行选中的末尾插入光标
- Ctrl+L：选择当前行
- Ctrl+Shift+L：选择当前选择的所有出现
- Ctrl+F2：选择当前单词的所有出现
- Shift+Alt+→：扩展选择
- Shift+Alt+←：缩小选择
- Shift+Alt+（拖动鼠标）：列（框）选择
- Ctrl+Shift+Alt+（箭头键）：列（框）选择
- Ctrl+Shift+Alt+PgUp/PgDn：列（框）选择页面上/下

## 富语言编辑
- Ctrl+Space, Ctrl+I：触发建议
- Ctrl+Shift+Space：触发参数提示
- Shift+Alt+F：格式化文档
- Ctrl+K Ctrl+F：格式化选择
- F12：转到定义
- Alt+F12：预览定义
- Ctrl+K F12：在侧边打开定义
- Ctrl+.：快速修复
- Shift+F12：显示引用
- F2：重命名符号
- Ctrl+K Ctrl+X：修剪尾随空格
- Ctrl+K M：更改文件语言

## 编辑器管理
- Ctrl+F4, Ctrl+W：关闭编辑器
- Ctrl+K F：关闭文件夹
- Ctrl+\：拆分编辑器
- Ctrl+1/2/3：聚焦到第1/2或第3编辑器组
- Ctrl+K Ctrl+←/→：聚焦到上一个/下一个编辑器组
- Ctrl+Shift+PgUp/PgDn：移动编辑器左/右
- Ctrl+K ←/→：移动活动编辑器组

## 文件管理
- Ctrl+N：新建文件
- Ctrl+O：打开文件...
- Ctrl+S：保存
- Ctrl+Shift+S：另存为...
- Ctrl+K S：保存全部
- Ctrl+F4：关闭
- Ctrl+K Ctrl+W：关闭全部
- Ctrl+Shift+T：重新打开关闭的编辑器
- Ctrl+K Enter：保持预览模式编辑器打开
- Ctrl+Tab：打开下一个
- Ctrl+Shift+Tab：打开上一个
- Ctrl+K P：复制活动文件路径
- Ctrl+K R：在资源管理器中显示活动文件
- Ctrl+K O：在新窗口/实例中显示活动文件

## 显示
- F11：切换全屏
- Shift+Alt+0：切换编辑器布局（水平/垂直）
- Ctrl+=/-：放大/缩小
- Ctrl+B：切换侧边栏可见性
- Ctrl+Shift+E：显示资源管理器/切换焦点
- Ctrl+Shift+F：显示搜索
- Ctrl+Shift+G：显示源代码控制
- Ctrl+Shift+D：显示调试
- Ctrl+Shift+X：显示扩展
- Ctrl+Shift+H：在文件中替换
- Ctrl+Shift+J：切换搜索详情
- Ctrl+Shift+U：显示输出面板
- Ctrl+Shift+V：打开Markdown预览
- Ctrl+K V：在侧边打开Markdown预览
- Ctrl+K Z：禅模式（按Esc退出）

## 调试
- F9：切换断点
- F5：开始/继续
- Shift+F5：停止
- F11/Shift+F11：步入/步出
- F10：步过
- Ctrl+K Ctrl+I：显示悬浮提示

## 集成终端
- Ctrl+`：显示集成终端
- Ctrl+Shift+`：创建新终端
- Ctrl+C：复制选择
- Ctrl+V：粘贴到活动终端
- Ctrl+↑/↓：向上/下滚动
- Shift+PgUp/PgDn：向上/下滚动页面
- Ctrl+Home/End：滚动到顶部/底部

## 键盘快捷键适用于Windows
其他操作系统的键盘快捷键和额外未分配的快捷键可在[aka.ms/vscodekeybindings](https://aka.ms/vscodekeybindings)查看。